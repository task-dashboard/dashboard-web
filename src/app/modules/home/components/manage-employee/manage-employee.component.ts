import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manage-employee',
  templateUrl: './manage-employee.component.html',
  styleUrls: ['./manage-employee.component.scss']
})
export class ManageEmployeeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  openNav() {
    document.getElementById("manageEmployee").style.width = "80%";
    document.getElementById("manageEmployee").style.border = "1px solid #707070";
  }
  
  closeNav() {
    document.getElementById("manageEmployee").style.width = "0";
    document.getElementById("manageEmployee").style.border = "unset";

  }


  public showNewUserWindow:Boolean=false
  showNewUser(){
    this.showNewUserWindow = !this.showNewUserWindow
  }

}
