import { Component, OnInit } from '@angular/core';

declare var $: any
@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {


  public data = {
    title:"asd",
    desc:"",
    etd:"",
    point:""
  }

  constructor() { }

  ngOnInit(): void {
  }

  async createTask(value){
    console.log(value)
  }

  openModel(){
    $("#createTask").modal("show");
  }

  closeModel(){
    $('#createTask').modal('hide');
  }


}
