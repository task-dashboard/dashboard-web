import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  private datatableElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  displayToConsole(datatableElement: DataTableDirective): void {
    datatableElement.dtInstance.then((dtInstance: DataTables.Api) => console.log(dtInstance));
  }

  constructor() { }

  ngOnInit(): void {
    this.dtOptions = {
      // ajax: (dataTablesParameters: any, callback) => {
        // that.http
        //   .get<DataTablesResponse>(
        //     `${environment.APIURL}/auction/${id}?search=${
        //       dataTablesParameters.search.value
        //     }&start=${dataTablesParameters.start}&length=${
        //       dataTablesParameters.length
        //     }&order_colomn=${
        //       dataTablesParameters.columns[dataTablesParameters.order[0].column]
        //         .data
        //     }&order_dir=${dataTablesParameters.order[0].dir}`,
        //     this.httpOptions
        //   )
        //   .subscribe((resp: any) => {
        //     that.auctionParticpant = resp.response.data;
        //     callback({
        //       recordsTotal: resp.response.records_total,
        //       recordsFiltered: resp.response.query_count,
        //       data: []
        //     });
          // });
      columns: [{
        title: 'ID',
        data: 'id'
      }, {
        title: 'First name',
        data: 'firstName'
      }, {
        title: 'Last name',
        data: 'lastName'
      }]
    };
  }

}
