import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { ManageEmployeeComponent } from './components/manage-employee/manage-employee.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent, CreateTaskComponent, ManageEmployeeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    DataTablesModule,
    FormsModule
  ]
})
export class HomeModule { }
