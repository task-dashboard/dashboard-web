import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('loginForm') myForm: NgForm;

  // password pattern
  selectedPattern: any = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&()^+-/=`~])[A-Za-z\\d@$!%*#?&()^+-/=`~]{8,}$";


  constructor(private router:Router) { }

  ngOnInit(): void {

  }

  doLogin(value){
    console.log(value)
  }

  async doSignUp(value) {
    console.log(value)
  }

  public signUpChange : Boolean = false
  changeSignUp(){
  this.signUpChange= !this.signUpChange
  }

  login(){
    this.router.navigate([''])
  }

  public showPassword: Boolean = false
  togglePassword() {
    this.showPassword = !this.showPassword;
  }

  public showConfirmPassword: Boolean = false
  toggleConfirmPassword() {
    this.showConfirmPassword = !this.showConfirmPassword;
  }

}
