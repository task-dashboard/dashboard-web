import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  async openCloseSideNav() {
    var element = document.getElementById("sidebar");
    element.classList.toggle("active");
  }

  openNav() {
    document.getElementById("editSidenav").style.width = "80%";
    document.getElementById("editSidenav").style.border = "1px solid #707070";
  }
  
  closeNav() {
    document.getElementById("editSidenav").style.width = "0";
    document.getElementById("editSidenav").style.border = "unset";

  }

  public showPass:Boolean =false
  showChangePassword(){
      this.showPass = !this.showPass
  }

}
